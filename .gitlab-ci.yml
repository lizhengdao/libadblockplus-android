image: "registry.gitlab.com/eyeo/docker/libadblockplus-android_gitlab-runner:202004.1"
stages:
  - unit_test
  - build_dependencies
  - build
  - device_tests
  - publish

# This updates the cache with shared dependencies
build_dependencies:
  stage: build_dependencies
  cache:
    key: $CI_COMMIT_REF_NAME
    policy: push
    paths:
      - libadblockplus/
      - buildtools/
  variables:
    GIT_SUBMODULE_STRATEGY: recursive
    WGET_FLAGS: "-q"
  script:
    - cd ./libadblockplus
    - make TARGET_OS=android ABP_TARGET_ARCH=arm Configuration=release get-prebuilt-v8
    - make TARGET_OS=android ABP_TARGET_ARCH=arm Configuration=release
    - make TARGET_OS=android ABP_TARGET_ARCH=arm64 Configuration=release get-prebuilt-v8
    - make TARGET_OS=android ABP_TARGET_ARCH=arm64 Configuration=release
    - make TARGET_OS=android ABP_TARGET_ARCH=ia32 Configuration=release get-prebuilt-v8
    - make TARGET_OS=android ABP_TARGET_ARCH=ia32 Configuration=release
    - make TARGET_OS=android ABP_TARGET_ARCH=x64 Configuration=release get-prebuilt-v8
    - make TARGET_OS=android ABP_TARGET_ARCH=x64 Configuration=release
    - cd ..

# This is a shared read-only cache definition
# which jobs can explicitly include to have build deps
.cache_readonly: &cache_readonly
  cache:
    key: $CI_COMMIT_REF_NAME
    paths:
      - libadblockplus/
      - buildtools/
    policy: pull

gradle_unit_tests:
  stage: unit_test
  script:
    - ./gradlew test
  artifacts:
    reports:
      junit: adblock-android/build/test-results/*/TEST-*.xml

build_libadblockplus-android:all:
  <<: *cache_readonly
  stage: build
  script:
    - ./gradlew clean assemble assembleAndroidTest
  # won't run for manual and scheduled; we'd like to run
  # `build_libadblockplus-android:all-and-test-release` instead
  except:
    - schedules
    - web
  artifacts:
    paths:
      - adblock-android-webviewapp/build/outputs/apk/release/adblock-android-webviewapp-release-unsigned.apk
      - adblock-android-webviewapp/build/outputs/apk/debug/adblock-android-webviewapp-debug.apk
      # The test APKs are used later in the device testing
      - adblock-android/build/outputs/apk/androidTest/abi_all/debug/adblock-android-abi_all-debug-androidTest.apk
      - adblock-android-webview/build/outputs/apk/androidTest/debug/adblock-android-webview-debug-androidTest.apk
      - adblock-android-webviewapp/build/outputs/apk/androidTest/debug/adblock-android-webviewapp-debug-androidTest.apk

# Builds additional release test apk for performance testing at nightly runs
build_libadblockplus-android:all-and-test-release:
  <<: *cache_readonly
  stage: build
  variables:
    PATH_TO_RELEASE_APK: "adblock-android-webview/build/outputs/apk/androidTest/release/adblock-android-webview-release-androidTest.apk"
  script:
    # we are building release apk as a separate call since we also need a debug test apk
    - ./gradlew clean assemble assembleAndroidTest && ./gradlew assembleAndroidTest -PtestBuildType=release
    # sign test apk with debug key
    - jarsigner -keystore ~/.android/debug.keystore -storepass android -keypass android ${PATH_TO_RELEASE_APK} androiddebugkey
  artifacts:
    paths:
      - adblock-android-webviewapp/build/outputs/apk/release/adblock-android-webviewapp-release-unsigned.apk
      - adblock-android-webviewapp/build/outputs/apk/debug/adblock-android-webviewapp-debug.apk
      # The test APKs are used later in the device testing
      - $PATH_TO_RELEASE_APK
      - adblock-android/build/outputs/apk/androidTest/abi_all/debug/adblock-android-abi_all-debug-androidTest.apk
      - adblock-android-webview/build/outputs/apk/androidTest/debug/adblock-android-webview-debug-androidTest.apk
      - adblock-android-webviewapp/build/outputs/apk/androidTest/debug/adblock-android-webviewapp-debug-androidTest.apk
      - adblock-android-benchmark/build/outputs/apk/androidTest/release/adblock-android-benchmark-release-androidTest.apk
  only:
    - schedules
    - web

# The remaining build_* jobs are only built on rc branches
# They are run only to test that the build works, we dont use the artefacts
.onlyrcbuilds: &onlyrc
  only:
    - /^.*-rc[0-9]+$/

build_libadblockplus-android:arm:
  stage: build
  <<: *cache_readonly
  <<: *onlyrc
  script:
    - ./gradlew clean assembleAbi_arm

build_libadblockplus-android:arm64:
  stage: build
  <<: *cache_readonly
  <<: *onlyrc
  script:
    - ./gradlew clean assembleAbi_arm64

build_libadblockplus-android:x86:
  stage: build
  <<: *cache_readonly
  <<: *onlyrc
  script:
    - ./gradlew clean assembleAbi_x86

build_libadblockplus-android:custom-jni:
  stage: build
  <<: *cache_readonly
  <<: *onlyrc
  script:
    - LIBABP_SHARED_LIBRARY_NAME=adblockplus ./gradlew clean assembleAbi_arm

build_libadblockplus-android:without-jni:
  stage: build
  # Custom cache definition for this job
  cache:
    key: $CI_COMMIT_REF_NAME
    paths:
      # Don't get dependencies because current dependencies are merely libadblockplus but we don't need it.
      # If the parameter is working correctly then the building should succeed without libadblockplus.
      - buildtools/
    policy: pull
  <<: *onlyrc
  script:
    - SKIP_JNI_COMPILATION=true ./gradlew clean assemble

# These are the instrumented tests that run on the real devices on https://saucelabs.com TestObject.
# @LargeTest tests are skipped in this job
testobject_tests:
  stage: device_tests
  script:
    - test -z ${TESTOBJ_PROJECT_KEY} && echo "echo TESTOBJ_PROJECT_KEY not set. Unable to run tests." && exit 1
    - test -f /opt/ci/runner.jar || wget https://s3.amazonaws.com/saucelabs-runner/v1.8/runner.jar -O /opt/ci/runner.jar
    # Docs for running tests on testobject:
    # https://wiki.saucelabs.com/display/DOCS/Command+Reference+for+Sauce+Runner+for+Real+Devices
    # Note - we start all tests concurrently and `wait` for them to finish. Non-zero exit codes are preserved.
    - |
      pids=""
      for APK in $(find . -name "*androidTest.apk")
        do echo "Testing $APK"
        java -jar /opt/ci/runner.jar espresso \
          --test $APK \
          --app ./adblock-android-webviewapp/build/outputs/apk/debug/adblock-android-webviewapp-debug.apk \
          --testname ${CI_JOB_URL} \
          --e notAnnotation androidx.test.filters.LargeTest \
          --apikey ${TESTOBJ_PROJECT_KEY} --datacenter EU &
        pids="$pids $!"
      done
      wait $pids
  artifacts:
    reports:
      junit: philll-adblockwebview-app-*.xml

# automated regression tests using Appium
regression_tests:
  stage: device_tests
  variables:
    # overridable automation repo; url uses CI_JOB_TOKEN for temporary authorization
    GIT_AUTOMATION_REPO: "https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/eyeo/sandbox/abp-webview-test-automation"
    # demo app debug apk
    TEST_APK: "adblock-android-webviewapp/build/outputs/apk/debug/adblock-android-webviewapp-debug.apk"
  script:
    # TESTOBJ_PROJECT_KEY should exist, testing it
    - test -z ${TESTOBJ_PROJECT_KEY} && echo "echo TESTOBJ_PROJECT_KEY not set. Unable to run tests." && exit 1
    # we are not using submodules, because automation is not a part of repository functionality
    # its rather a standalone set of scripts
    - git clone $GIT_AUTOMATION_REPO automation
    # `./gradlew cucumber` launches cucumber executor that then launches Appium client
    - cd automation && ./gradlew cucumber

# FilterEngine memory benchmarking
# This job runs @LargeTest instrumented tests as well as all the others.
memory_benchmark:
  tags:
    # This is a Raspberry PI with an attached rooted device that is located at the office.
    - "android-pi"
  stage: device_tests
  script:
    - chmod +x ./tools/run_benchmark.py && ./tools/run_benchmark.py
    # performance tests: only the @PerformanceTest annotation
    - adb shell am instrument -w -r \
      -e debug false \
      -e size large \
      -e annotation org.adblockplus.libadblockplus.android.webview.PerformanceTest \
      org.adblockplus.libadblockplus.android.webview.test/androidx.test.runner.AndroidJUnitRunner
  after_script:
    - adb uninstall org.adblockplus.libadblockplus.android.webview.test
    - adb kill-server
  artifacts:
      paths:
        - adblock-android-webview/build/outputs/memory_benchmark/
      reports:
        metrics: metrics.txt
  only:
    - schedules
    - web

# FilterEngine performance benchmarking
performance_benchmark:
  tags:
    # This is a Raspberry PI with an attached rooted device that is located at the office.
    - "android-pi"
  stage: device_tests
  variables:
    LOCK_CLOCK_FILE: "/data/local/tmp/lockClocks.sh"
    BENCHMARK_PULL_DIR: "adblock-android-benchmark/build/outputs/connected_android_test_additional_output"
  script:
    # Gradle feels heavy for Raspberry Pi
    # so using "adb" commands directly instead of "gradlew ..." commands:
    # (see https://developer.android.com/studio/profile/run-benchmarks-in-ci)
    # ./gradlew lockClocks
    - adb install adblock-android-benchmark/build/outputs/apk/androidTest/release/adblock-android-benchmark-release-androidTest.apk
    - adb push adblock-android-benchmark/scripts/lockClocks.sh $LOCK_CLOCK_FILE
    - adb shell su - -c $LOCK_CLOCK_FILE
    - adb shell rm $LOCK_CLOCK_FILE
    # ./gradlew :adblock-android-benchmark:connectedCheck
    - adb shell am instrument -w -r \
      -e androidx.benchmark.suppressErrors LOW-BATTERY \
      -e no-isolated-storage 1 \
      -e androidx.benchmark.output.enable true \
      org.adblockplus.libadblockplus.benchmark.test/androidx.benchmark.junit4.AndroidBenchmarkRunner
    # collect benchmarking results raw file
    - mkdir -p $BENCHMARK_PULL_DIR
    - adb pull /storage/emulated/0/Download/org.adblockplus.libadblockplus.benchmark.test-benchmarkData.json $BENCHMARK_PULL_DIR/benchmarkData.json
  after_script:
    - adb uninstall org.adblockplus.libadblockplus.benchmark.test
    # ./gradle unlockClocks
    - adb shell reboot
  artifacts:
    paths:
      - $BENCHMARK_PULL_DIR
  only:
    - schedules
    - web

.publish_template: &publish_template
  stage: publish
  <<: *cache_readonly
  script:
    - |
      if [[ ${DRY_RUN:-true} == "true" ]] ; then
        echo 'Note: $DRY_RUN is currently "true", run pipeline manually and set to "false" to actually publish to bintray'
      fi
    - ./gradlew bintrayUpload -PbintrayUser=${BINTRAY_USERNAME:-unset} -PbintrayKey=${BINTRAY_KEY:-unset} -PdryRun=${DRY_RUN:-true}

publish_rc_to_bintray:
  <<: *publish_template
  only:
    variables:
      - $RC
  before_script:
    - echo "Publishing release candidate ${RC}"
    # Append '-rcX' to the version to publish
    - sed -i -e "/moduleVersion/s/'$/-rc${RC}'/g" build.gradle

publish_to_bintray:
  <<: *publish_template
  stage: publish
  only:
    - tags
